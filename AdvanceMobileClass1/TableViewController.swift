//
//  TableViewController.swift
//  AdvanceMobileClass1
//
//  Created by David Guzman on 2018-03-07.
//  Copyright © 2018 David Guzman. All rights reserved.
//

import UIKit

class TableViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var whatILearned = ["One thing", "Another thing","Last thing"]
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self

        print(whatILearned)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    

}
extension TableViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return whatILearned.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let thingILearned = whatILearned[indexPath.row]
        let cell = UITableViewCell()
        cell.textLabel?.text = thingILearned
        return cell
    }
    
}
extension TableViewController : UITableViewDelegate{
    	
}
